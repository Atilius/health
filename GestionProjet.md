# Gestion de projet

## Backlog
* Ajouter une masse par utilisateur
* Implémenter le sommeil
* Implémenter la tension artérielle
* Exporter la tension artérielle

## À faire
* Lier le back et le front

## En cours
* Faire la BDD
* Faire le backend
* Gestion des profils

## Code Review

## Test

## Terminé
* Création du front
