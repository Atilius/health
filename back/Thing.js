const mongoose = require('mongoose');

const thingSchema = mongoose.Schema({
    name: {type: String, require: true},
    lastname: {type: String, require: false},
    age: {type: Number, require: true},
    weight: {type: Object, require: true},
    height: {type: Number, require: true},
    wrist: {type: Number, require: true},
    password: {type: String, require: true},
});

module.exports = mongoose.model('Thing', thingSchema);