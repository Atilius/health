const express = require("express");
const mongoose = require("mongoose");
const Thing = require("./Thing");
const bcrypt = require("bcrypt");
const { base } = require("./Thing");

const app = express();
const uri =
  "mongodb+srv://Alexandre:gXp8scS2BiVYKH3@cluster0.xkpmc.mongodb.net/myFirstDatabase?retryWrites=true&w=majority";

mongoose
  .connect(uri, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => console.log("Connexion à MongoDB réussie !"))
  .catch((error) => console.log("Connexion à MongoDB échouée !"));

app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content, Accept, Content-Type, Authorization"
  );
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, PUT, DELETE, PATCH, OPTIONS"
  );
  next();
});

// app.post('/api/stuff', (req, res, next) => {
//   delete req.body._id;
//   const thing = new Thing({
//     ...req.body
//   });
//   thing.save()
//     .then(() => res.status(201).json({ message: 'Objet enregistré !'}))
//     .catch(error => res.status(400).json({ error }));
// });

app.use(express.json());

app.get("/api/addpersonn", async (req, res, next) => {
  const thing = new Thing({
    name: "Alexandre",
    lastname: "Ansel",
    age: 22,
    weight: {
      value: [77.1, 77.8, 76.3, 76.7, 76.3, 76.8, 77.1, 76.3, 76.1, 77.6, 77.2],
      date: [
        new Date(2021, 1, 2),
        new Date(2021, 1, 20),
        new Date(2021, 2, 15),
        new Date(2021, 2, 20),
        new Date(2021, 2, 27),
        new Date(2021, 2, 28),
        new Date(2021, 3, 10),
        new Date(2021, 3, 15),
        new Date(2021, 3, 20),
        new Date(2021, 3, 24),
        new Date(2021, 3, 26),
      ],
    },
    height: 182,
    wrist: 16.75,
    password: await bcrypt.hash("toto", 10),
  });
  thing
    .save()
    .then(() => res.status(201).json({ message: "Personne enregistré !" }))
    .catch((error) => res.status(400).json({ error }));
});

app.post("/api/user/login", async (req, res, next) => {
  const user = await Thing.findOne({ name: req.body.username });
  if (user === null) {
    return res.status(401).json(false);
  }
  const valid = await bcrypt.compare(req.body.password, user.password);
  if (!valid) {
    return res.status(401).json(false);
  }
  res.status(200).json(true);
});

app.get("/api/data/:data/:username", async (req, res, next) => {
  const user = await Thing.findOne({ name: req.params.username });
  // console.log(user);
  if (user === null) {
    return res.status(401).json(false);
  }
  if (req.params.data === "all") {
    // delete user.password;
    // console.log(user);
    return res.status(200).json(user);
  }
  const data = user.get(req.params.data);
  if (data === undefined) {
    return res.status(401).json(false);
  }
  res.status(200).json(data);
});

app.use("/api/all", (req, res, next) => {
  Thing.find()
    .then((things) => res.status(200).json(things))
    .catch((error) => res.status(400).json({ error }));
});

module.exports = app;
