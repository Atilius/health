import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));

export default function ChangeUser() {
  const classes = useStyles();
  const [age, setAge] = React.useState("");

  const handleChange = (event) => {
    setAge(event.target.value);
  };

  return (
    <div>
      <FormControl className={classes.formControl}>
        <InputLabel id="demo-simple-select-label">Profil</InputLabel>
        <Select className="dark-theme" value={age} onChange={handleChange}>
          <MenuItem value={"alexandre"}>Alexandre</MenuItem>
          <MenuItem value={"vincent"}>Vincent</MenuItem>
          <MenuItem value={"aurore"}>Aurore</MenuItem>
        </Select>
      </FormControl>
    </div>
  );
}
