import React from 'react';
import './css/TableProfile.css';
import { IMC, idealWeight } from './Calculation';
import GaugeIndex from './GaugeIndex';

export default class TableProfile extends React.Component {
	// constructor(props) {
	// 	super(props);
	// 	console.log(props.user);
	// }
	currentWeight() {
		// console.log(this.props.user);
		const weights = this.props.user.weight.value;
		const value = weights[weights.length - 1];
		return value;
	}

	render() {
		return (
			<table className="tableProfile">
				<tbody>
					<tr>
						<th>Age</th>
						<td>{this.props.user.age} ans</td>
					</tr>
					<tr>
						<th>Taille</th>
						<td>{this.props.user.height} cm</td>
					</tr>
					<tr>
						<th>Masse</th>
						<td>{this.currentWeight()} kg</td>
					</tr>
					<tr>
						<th>IMC</th>
						<td>
							<GaugeIndex
								className="gauge"
								value={IMC(this.currentWeight(), this.props.user.height)}
							/>
						</td>
					</tr>
					<tr>
						<th>Idéal</th>
						<td>
							{idealWeight(this.props.user.height, this.props.user.wrist)} kg
						</td>
					</tr>
				</tbody>
			</table>
		);
	}
}

// export default function TableProfile(this.props) {
//   // async function imc() {
//   //     const username = localStorage.getItem('username')
//   //     let response = await fetch(`http://localhost:4000/api/data/weight/${username}`)
//   //     const weight = await response.json()
//   //     response = await fetch(`http://localhost:4000/api/data/height/${username}`)
//   //     const height = await response.json()
//   //     IMC(weight.value[weight.value.length - 1], height)
//   // }

//   async function imc() {
//     // const username = localStorage.getItem('username')
//     const weight = await getBDD("weight");
//     const height = await getBDD("height");
//     // console.log(weight, height)
//     // return IMC(currentWeight(), height)
//   }

//   async function getBDD(data) {
//     const username = localStorage.getItem("username");
//     try {
//       const res = await fetch(
//         `http://localhost:4000/api/data/${data}/${username}`
//       );
//       return await res.json();
//     } catch (error) {
//       console.log(error);
//     }
//   }

//   const age = getBDD("age");

//   let height;
//   getBDD("height").then((res) => (height = res));
//   console.log(height);

//   const weight = getBDD("weight");

//   const wrist = getBDD("wrist");

//   return (
//     <table className="tableProfile">
//       <tr>
//         <th>Age</th>
//         <td>{age} ans</td>
//       </tr>
//       <tr>
//         <th>Taille</th>
//         <td>{height} cm</td>
//       </tr>
//       {/* <tr><th>Masse</th><td>{currentWeight()} kg</td></tr> */}
//       {/* <tr><th>IMC</th><td><GaugeIndex className="gauge" value={imc}/></td></tr>
//             <tr><th>Idéal</th><td>{idealWeight(height, wrist)} kg</td></tr> */}
//     </table>
//   );
// }
