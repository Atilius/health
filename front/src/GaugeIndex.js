import GaugeChart from "react-gauge-chart";
import "./css/GaugeIndex.css";

export default function GaugeIndex(props) {
  const pourcent = props.value / 20 - 0.75;

  const chartStyle = {
    // height: 100,
    width: 200,
  };

  return (
    <div className="gauge">
      <GaugeChart
        style={chartStyle}
        // nrOfLevels={450}
        arcsLength={[3.5, 6.5, 5, 5]}
        colors={["#F6E810", "#5AD400", "#F6E810", "#EA4228"]}
        percent={pourcent}
        arcPadding={0.02}
        // formatTextValue={value => value}
        formatTextValue={(value) => ((value / 100 + 0.75) * 20).toFixed(1)}
      />
    </div>
  );
}
