export async function getBDD(data) {
  const username = localStorage.getItem("username");
  try {
    const res = await fetch(
      `http://localhost:4000/api/data/${data}/${username}`
    );
    return await res.json();
  } catch (error) {
    console.log(error);
  }
}
