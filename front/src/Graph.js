import './css/Graph.css';
import { Chart } from 'react-charts';
import React from 'react';

export default function Graph(props) {
	function prepareData() {
		let result = [];
		for (let index = 0; index < props.weight.value.length; index++) {
			result.push({
				x: new Date(props.weight.date[index]),
				y: props.weight.value[index],
			});
		}

		return [
			{
				label: props.name,
				data: result,
			},
		];
	}

	const axes = React.useMemo(
		() => [
			{ primary: true, type: 'time', position: 'bottom' },
			{ type: 'linear', position: 'left', color: 'white' },
		],
		[]
	);

	const options = React.useMemo(() => [
		{
			scales: {
				xAxes: [
					{
						type: 'time',
						time: {
							unit: 'day',
						},
					},
				],
			},
		},
	]);

	const lineChart = (
		// A react-chart hyper-responsively and continuously fills the available
		// space of its parent element automatically
		<div className="graphique">
			<Chart data={prepareData()} axes={axes} options={options} tooltip dark />
		</div>
	);

	return lineChart;
}
