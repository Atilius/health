export function IMC(weight, height) {
	let result = (weight / Math.pow(height, 2)) * 10000;
	result = result.toFixed(1);
	return result;
}

export function idealWeight(height, wrist) {
	const result = (height - 100 + 4 * wrist) / 2;
	return result.toFixed(1);
}
