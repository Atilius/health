import React from 'react';
import { Redirect } from 'react-router-dom';
import { NavLink } from 'react-router-dom';

import './css/Dashboard.css';

import TableProfile from './TableProfile';
import Graph from './Graph';

import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import Tooltip from '@material-ui/core/Tooltip';

class Dashboard extends React.Component {
	constructor(props) {
		super(props);
		this.state = { isLoaded: false, user: undefined };
	}

	async componentDidMount() {
		const username = localStorage.getItem('username');

		while (username === undefined) {}
		let user = await fetch(`http://localhost:4000/api/data/all/${username}`, {
			method: 'GET',
			headers: { 'Content-type': 'application/json; charset=UTF-8' },
		});
		this.setState({ isLoaded: true, user: await user.json() });
	}

	logoff() {
		localStorage.setItem('loggedIn', 'false');
		localStorage.setItem('username', 'undefind');
	}

	// const users = [
	//   {
	//     name: "Alexandre",
	//     lastname: "Ansel",
	//     age: 22,
	//     weight: {
	//       value: [
	//         77.1,
	//         77.8,
	//         76.3,
	//         76.7,
	//         76.3,
	//         76.8,
	//         77.1,
	//         76.3,
	//         76.1,
	//         77.6,
	//         77.2,
	//       ],
	//       date: [
	//         new Date(2021, 1, 2),
	//         new Date(2021, 1, 20),
	//         new Date(2021, 2, 15),
	//         new Date(2021, 2, 20),
	//         new Date(2021, 2, 27),
	//         new Date(2021, 2, 28),
	//         new Date(2021, 3, 10),
	//         new Date(2021, 3, 15),
	//         new Date(2021, 3, 20),
	//         new Date(2021, 3, 24),
	//         new Date(2021, 3, 26),
	//       ],
	//     },
	//     height: 182,
	//     wrist: 16.75,
	//   },
	//   {
	//     name: "Vincent",
	//     lastname: "Danels",
	//     age: 43,
	//     weight: [85, 94.3],
	//     height: 183,
	//     wrist: 18,
	//   },
	//   {
	//     name: "Aurore",
	//     lastname: "Danels",
	//     age: 17,
	//     weight: {
	//       value: [63.4, 62.8, 67.5],
	//       // date: ["02-01-2021","20-03-2021"]
	//       date: [
	//         new Date(2021, 3, 15),
	//         new Date(2021, 3, 20),
	//         new Date(2021, 3, 27),
	//       ],
	//       // date: ["144","145"]
	//     },
	//     height: 175,
	//     wrist: 15,
	//   },
	// ];

	// const user = users[0]

	render() {
		if (localStorage.getItem('loggedIn') !== 'true') {
			return <Redirect to="/login" />;
		}

		const { isLoaded, user } = this.state;
		if (!isLoaded) {
			return (
				<div className="App">
					<div className="Profile">
						<h1>Chargement...</h1>
					</div>
				</div>
			);
		} else {
			return (
				<div className="App">
					<div className="Profile">
						<h1>{user.name}</h1>

						{/* <Tooltip title="Ajouter donnée">
							<AddCircleOutlineIcon fontSize="large" />
						</Tooltip> */}

						<NavLink
							to="/login"
							style={{
								textDecoration: 'none',
								color: 'white',
								marginLeft: '3%',
							}}
						>
							<Tooltip title="Déconnexion">
								<ExitToAppIcon fontSize="large" onClick={this.logoff} />
							</Tooltip>
						</NavLink>

						<TableProfile user={user} />
					</div>
					<Graph weight={user.weight} name="Masse" />
				</div>
			);
		}
	}
}

export default Dashboard;
