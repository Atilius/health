import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import Login from "./Login";
import Dashboard from "./Dashboard";

export default function Navigator() {
  return (
    <Switch>
      <Route exact path="/">
        <Redirect to="/login" />
      </Route>

      <Route exact path="/main">
        <Dashboard />
      </Route>

      <Route exact path="/login">
        <Login />
      </Route>
    </Switch>
  );
}
